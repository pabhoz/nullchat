-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema nullchat
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `nullchat` ;

-- -----------------------------------------------------
-- Schema nullchat
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `nullchat` DEFAULT CHARACTER SET utf8 ;
USE `nullchat` ;

-- -----------------------------------------------------
-- Table `nullchat`.`Usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `nullchat`.`Usuario` ;

CREATE TABLE IF NOT EXISTS `nullchat`.`Usuario` (
  `id_usuario` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `nullchat`.`Snap`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `nullchat`.`Snap` ;

CREATE TABLE IF NOT EXISTS `nullchat`.`Snap` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `owner` INT NOT NULL,
  `viewed` TINYINT(1) NOT NULL DEFAULT 0,
  `foto` LONGBLOB NOT NULL,
  `historia` TINYINT(1) NOT NULL DEFAULT 0,
  `fecha` DATETIME NOT NULL,
  `duracion` INT NOT NULL DEFAULT 5,
  PRIMARY KEY (`id`),
  INDEX `fk_Snap_Usuario1_idx` (`owner` ASC),
  CONSTRAINT `fk_Snap_Usuario1`
    FOREIGN KEY (`owner`)
    REFERENCES `nullchat`.`Usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `nullchat`.`amigos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `nullchat`.`amigos` ;

CREATE TABLE IF NOT EXISTS `nullchat`.`amigos` (
  `id_usuario` INT NOT NULL,
  `id_amigo` INT NOT NULL,
  PRIMARY KEY (`id_usuario`, `id_amigo`),
  INDEX `fk_amigos_Usuario2_idx` (`id_amigo` ASC),
  CONSTRAINT `fk_amigos_Usuario1`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `nullchat`.`Usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_amigos_Usuario2`
    FOREIGN KEY (`id_amigo`)
    REFERENCES `nullchat`.`Usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `nullchat`.`destinatarios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `nullchat`.`destinatarios` ;

CREATE TABLE IF NOT EXISTS `nullchat`.`destinatarios` (
  `id_destinatario` INT NOT NULL,
  `id_snap` INT NOT NULL,
  PRIMARY KEY (`id_destinatario`, `id_snap`),
  INDEX `fk_destinatarios_Usuario1_idx` (`id_destinatario` ASC),
  INDEX `fk_destinatarios_Snap1_idx` (`id_snap` ASC),
  CONSTRAINT `fk_destinatarios_Usuario1`
    FOREIGN KEY (`id_destinatario`)
    REFERENCES `nullchat`.`Usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_destinatarios_Snap1`
    FOREIGN KEY (`id_snap`)
    REFERENCES `nullchat`.`Snap` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
