<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of newPHPClass
 *
 * @author pabhoz
 */
class Snap {
    //put your code here
    protected static $table = "Snap";
    private $id;
    private $owner;
    private $viewed;
    private $foto;
    private $historia;
    private $fecha;
    private $duracion;
    
    private $has_many = array(
        'destinatarios' => array(
            'class' => 'destinatarios',
            'my_key' => 'id',
            'other_key' => 'id',
            'join_as' => 'id_snap',
            'join_with' => 'id_destinatario',
            'data' => array( 
                'valor' => null
                ),
            'join_table' => 'destinatarios'
        )
    );

    function __construct($id, $owner, $viewed, $foto, $historia, $fecha, $duracion) {
        $this->id = $id;
        $this->owner = $owner;
        $this->viewed = $viewed;
        $this->foto = $foto;
        $this->historia = $historia;
        $this->fecha = $fecha;
        $this->duracion = $duracion;
    }

    
     public function getMyVars() {
        return get_object_vars($this);
    }
    
    function getId() {
        return $this->id;
    }

    function getOwner() {
        return $this->owner;
    }

    function getViewed() {
        return $this->viewed;
    }

    function getFoto() {
        return $this->foto;
    }

    function getHistoria() {
        return $this->historia;
    }

    function getFecha() {
        return $this->fecha;
    }

    function getDuracion() {
        return $this->duracion;
    }

    function getHas_many() {
        return $this->has_many;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setOwner($owner) {
        $this->owner = $owner;
    }

    function setViewed($viwed) {
        $this->viewed = $viwed;
    }

    function setFoto($foto) {
        $this->foto = $foto;
    }

    function setHistoria($historia) {
        $this->historia = $historia;
    }

    function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    function setDuracion($duracion) {
        $this->duracion = $duracion;
    }

    function setHas_many($has_many) {
        $this->has_many = $has_many;
    }
} 