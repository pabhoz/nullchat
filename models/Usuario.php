<?php

/**
 * Description of Usuario
 *
 * @author pabhoz
 */
class Usuario extends Model {

    protected static $table = "Usuario";
    private $id;
    private $username;
    private $password;
    
    private $has_many = array(
        'amigos' => array(
            'class' => 'Usuario',
            'my_key' => 'id',
            'other_key' => 'id',
            'join_as' => 'id_usuario',
            'join_with' => 'id_amigo',
            'data' => array( 
                'valor' => null
                ),
            'join_table' => 'amigos'
        )
    );
    
    function __construct($id, $username, $password) {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
    }

    
    public function getMyVars() {
        return get_object_vars($this);
    }
    
    function getId() {
        return $this->id;
    }

    function getUsername() {
        return $this->username;
    }

    function getPassword() {
        return $this->password;
    }

    function getHas_many() {
        return $this->has_many;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function setPassword($password) {
        $this->password = $password;
    }
    
    function setHas_many($has_many) {
        $this->has_many = $has_many;
    }

    private function getMyFriends(){
        self::$table = "amigos";
        return $this->search("id_usuario = ".$this->getId());
    }
    
    public function isMyFriend($usr){
        $amigos = $this->getMyFriends();
        $response = false;
        foreach ($amigos as $amigo) {
            if($usr->getId() == $amigo["id_amigo"]){
                $response = true;
                return $response;
            }
        }
        return $response;
    }

}
