<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Usuario: <?php print($this->user->getUsername()); ?></title>
	<link rel="stylesheet" href="">
</head>
<body>
	<ul>
		<li>
			Usuario: <?php print($this->user->getUsername()); ?>
		</li>
		<li>
			Nombre: <?php print($this->user->getNombre()); ?>
		</li>
		<li>
			Correo: <?php print($this->user->getCorreo()); ?>
		</li>
		<li>
			Celular: <?php print($this->user->getCelular()); ?>
		</li>
	</ul>
</body>
</html>