<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuario_controller
 *
 * @author pabhoz
 */
class Usuario_controller extends Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function userInfo($id){
        $usr = Usuario::getById($id);
        $this->view->user = $usr;
        $this->view->render($this,"userInfo");
    }
}
